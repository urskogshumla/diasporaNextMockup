function toggleFolded(e) {
	if (e.style.height == "auto"){
        e.style.height = "0px";
        e.previousElementSibling.getElementsByClassName("foldedMenuCaret")[0].style.transform = "";
    }else
		e.style.height = "auto";
        e.previousElementSibling.getElementsByClassName("foldedMenuCaret")[0].style.transform = "";
    
    e.previousElementSibling.getElementsByClassName("foldedMenuCaret")[0].classList.toggle("rotated-90");
}

function toggleDropDown(e, s) {
	if (e.style.opacity == "1"){
		e.style.opacity = "0";
		if (s) diasporaLogoRotate(0);
	}
	else {
		e.style.opacity = "1";
		if (s) diasporaLogoRotate(1);
	}
}
function setToRead(e) {
    toggleReadUnread(e, true, false);
}
function setToUnread(e) {
    toggleReadUnread(e, true, true);
}
function toggleReadUnread(e, r, f) {
    if (!e.classList.contains("notificationItem")) {
        e = e.parentElement;
    }
    if (typeof r == "undefined")
        r = false;
    if (r) { // Init
        if (e.classList.contains("notificationUnread"))
            r = true;
        else
            r = false;
    }
    else { // Toggle
        if (e.classList.contains("notificationUnread"))
            r = false;
        else
            r = true;
    }
    if (typeof f != "undefined")
        r = f;
    if (r) 
        e.classList.add("notificationUnread");
    else
        e.classList.remove("notificationUnread");
    
    //e = e.firstElementChild.firstElementChild;
    if (!r) {
        e.getElementsByClassName("irisContour")[0].firstElementChild.firstElementChild.classList.add("hidden");
        e.getElementsByClassName("irisContour")[1].firstElementChild.firstElementChild.classList.add("hidden");
        //e.style.opacity = "0";
        //e.nextElementSibling.style.opacity = "1";
    }
    else {
        e.getElementsByClassName("irisContour")[0].firstElementChild.firstElementChild.classList.remove("hidden");
        e.getElementsByClassName("irisContour")[1].firstElementChild.firstElementChild.classList.remove("hidden");
        //e.style.opacity = "1";
        //e.nextElementSibling.style.opacity = "0";
    }
    notUpdate(document.getElementsByClassName("notificationUnread").length);
    if (document.getElementsByClassName("notificationUnread").length != 0)
        e.parentElement.parentElement.parentElement.getElementsByClassName("badge")[0].innerHTML = document.getElementsByClassName("notificationUnread").length + "";
    else
        e.parentElement.parentElement.parentElement.getElementsByClassName("badge")[0].innerHTML = "";
}

function notificationClick(e, arg) {
    if (e.classList.contains("notificationItem")) {
        if (e.classList.contains("notificationUnread"))
            setToRead(e.firstElementChild);
    }
    else {
        if (!e.parentElement.classList.contains("notificationUnread")){
            setTimeout(function () { toggleReadUnread(this) }.bind(e),1);
        }
    }
}

function initReadUnread() {
    var e = document.getElementsByClassName("notificationItem");
    for (var i = 0; i < e.length; i += 1) {
        toggleReadUnread(e[i].firstElementChild, true);
    }
}