var notMoons = document.getElementsByClassName("notMoon"), notCount = 0, notSpread = 360, notMoonMax = notMoons.length;
function cut(pt) {
	//Cuts the value of a 360 degree loop.
	while (pt < 0) { pt += 360; }
	while (pt >= 360) { pt += -360; }
	return pt;
}

function divideNumber(pa) {
	//Defaults to one is the number of moons is zero
	if (pa == 0) return 1;
	return pa;
}

function notUpdate(pNum) {
	//Updates notifications symbol to pNum number of notifications.
	if (typeof pNum == "undefined") pNum = notCount;
	var vbefore = notCount;
	notCount = pNum;
	if (notCount == vbefore) return "No need to update.";
	if (notCount > notMoonMax) {
		document.getElementsByClassName("notPlanet")[0].firstElementChild.style.background = "white";
		document.getElementsByClassName("notPlanet")[0].style.transform = "scale(0.8)";
	}
	else {
		document.getElementsByClassName("notPlanet")[0].firstElementChild.style.background = "transparent";
		document.getElementsByClassName("notPlanet")[0].style.transform = "scale(1)";
	}
	for (var i = 0; i < notMoons.length; i += 1) {
		notMoons[i].style.transform = ""; //Get extra css like the webkit stuff in the right order; super buggy hehe.
		if (notCount != 0 && notCount <= notMoonMax)
			notMoons[i].style.transform = "rotate(" + (i * (notSpread / divideNumber(notCount)) - notCount * 55) + "deg)";
		if (i < notCount && notCount <= notMoonMax) 
			notMoons[i].style.opacity = "0.6";
		else 
			notMoons[i].style.opacity = "0";
		notMoons[i].style.transform += "scale(" + Math.ceil(notMoons[i].style.opacity)*0.6 + 0.4 + ")";
	}
}

//Set initvalues for moons
for (var i = 0; i < notMoons.length; i += 1) {
	notMoons[i].style.transform = "rotate(" + (Math.random() * 360) + "deg)";
	notMoons[i].style.webkitTransform = "rotate(" + (Math.random() * 360) + "deg)";
	notMoons[i].style.opacity = "0";
}