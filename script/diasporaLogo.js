function diasporaLogoRotate(x) {
	if (typeof x == "undefined") x = 0;
	for (var i = 0; i < document.getElementsByClassName("diasporaLogo").length; i += 1) {
		for (var j = 0; j < 5; j += 1) {
			document.getElementsByClassName("diasporaLogo")[i].children[j].style.transform = "rotate(" + (x * 72 + j * 72) + "deg)";
            if (document.getElementsByClassName("diasporaLogo")[i].classList.contains("diasporaLogoInner"))
                document.getElementsByClassName("diasporaLogo")[i].children[j].firstElementChild.style.height = "calc(55% + -1.4px)";
            else
                document.getElementsByClassName("diasporaLogo")[i].children[j].firstElementChild.style.height = "55%";
			document.getElementsByClassName("diasporaLogo")[i].children[j].firstElementChild.style.borderTopLeftRadius = "0%";
			document.getElementsByClassName("diasporaLogo")[i].children[j].firstElementChild.style.borderTopRightRadius = "0%";
		}
		//document.getElementsByClassName("diasporaLogo")[i].style.opacity = "0.6";

	}
}
