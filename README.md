## Diaspora Next

This mockup explores a playful approach on design for the [diaspora 
social network](https://diasporafoundation.org) UI. Its purpose is to rethink the current design in a 
more modern, straightforward and device independent way.

### Some small goals

* Provide easily navigated menus
* Responsive design
* Consistency in features and design concepts
* A playfulness along with visual eyecandy

### People involved right now:

Ivan Morén; design, dev  
Paul Greindl; design, testing, dev
